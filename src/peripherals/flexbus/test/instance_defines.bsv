`define ADDR 32
`define PADDR 32
`define DATA 64
`define Reg_width 64
`define USERSPACE 0

// TODO: work out if these are needed
`define PWM_AXI4Lite
`define PRFDEPTH 6
`define VADDR 39
`define DCACHE_BLOCK_SIZE 4
`define DCACHE_WORD_SIZE 8
`define PERFMONITORS                            64
`define DCACHE_WAYS 4
`define DCACHE_TAG_BITS 20      // tag_bits = 52
`define PLIC
	`define PLICBase		'h0c000000
	`define PLICEnd		'h10000000
`define INTERRUPT_PINS 64

`define BAUD_RATE 130
`ifdef simulate
  `define BAUD_RATE 5 //130 //
`endif

`define byte_offset 2
`define RV64

`define TESTING
