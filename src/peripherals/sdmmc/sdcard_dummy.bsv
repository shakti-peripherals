/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

-------------------------------------------------------------------
*/

package sdcard_dummy;
  `define SDBUSWIDTH 4
  `include "instance_defines.bsv"
  import GetPut::*;
  import ClockDiv::*;
  import ConcatReg::*;
	import Semi_FIFOF::*;
	import BUtils ::*;
 	import AXI4_Lite_Types::*;

  interface Ifc_sdcard_dummy;
	  interface AXI4_Lite_Slave_IFC#(`PADDR, `DATA, `USERSPACE) slave;
    interface Get#(Bit#(1)) cmd;
    interface Get#(Bit#(1)) clk;
    interface Get#(Bit#(`SDBUSWIDTH)) out;
    interface Get#(Bit#(`SDBUSWIDTH)) out_en;
    interface Put#(Bit#(`SDBUSWIDTH)) in;
  endinterface

  (*synthesize*)
  module mksdcard_dummy(Ifc_sdcard_dummy);
	  	AXI4_Lite_Slave_Xactor_IFC#(`PADDR,`DATA, `USERSPACE)
                s_xactor<-mkAXI4_Lite_Slave_Xactor();

      Reg#(Bit#(1)) rg_cmd <- mkReg(0);
      Reg#(Bit#(1)) rg_clk <- mkReg(0);
      Reg#(Bit#(`SDBUSWIDTH)) rg_out <- mkReg(0);
      Reg#(Bit#(`SDBUSWIDTH)) rg_out_en <- mkReg(0);
      Reg#(Bit#(`SDBUSWIDTH)) rg_in <- mkReg(0);

      interface cmd = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_cmd;
        endmethod
      endinterface;

      interface clk = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_clk;
        endmethod
      endinterface;

      interface out_en = interface Get
        method ActionValue#(Bit#(`SDBUSWIDTH)) get;
          return rg_out_en;
        endmethod
      endinterface;

      interface out = interface Get
        method ActionValue#(Bit#(`SDBUSWIDTH)) get;
          return rg_out;
        endmethod
      endinterface;

      interface in = interface Put
        method Action put(Bit#(`SDBUSWIDTH) in);
          rg_in <= in;
        endmethod
      endinterface;

    interface slave=s_xactor.axi_side;

  endmodule

endpackage
