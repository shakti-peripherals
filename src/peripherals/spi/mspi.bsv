/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

*  Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------
*/

package mspi;

    import AXI4_Lite_Types   :: *;
    import AXI4_Lite_Fabric  :: *;
	import GetPut::*;
	import mqspi::*;
    `include "instance_defines.bsv"


    (*always_ready, always_enabled*)
    interface SPI_out;
        interface Get#(Bit#(1)) clk_o;
        method Bit#(9) io0_sdio_ctrl;
        method Bit#(9) io1_sdio_ctrl;
        // index 0 is MOSI, index 1 is MISO.
        interface Get#(Bit#(2)) io_out;
        interface Get#(Bit#(2)) io_out_en;
        interface Put#(Bit#(2)) io_in;
        interface Get#(Bit#(1)) ncs_o;
    endinterface

    interface Ifc_mspi;
        interface SPI_out out;
		interface AXI4_Lite_Slave_IFC#(`PADDR,`Reg_width,`USERSPACE) slave;
        // 0=TOF, 1=SMF, 2=Threshold, 3=TCF, 4=TEF 5 = request_ready
		method Bit#(6) interrupts; 
	endinterface


	(*synthesize*)
	module mkmspi(Ifc_mspi);
	
    Ifc_mqspi qspi <- mkmqspi();

    interface out = interface SPI_out
    	method Bit#(9) io0_sdio_ctrl;
	    	return qspi.out.io0_sdio_ctrl;
    	endmethod
    	method Bit#(9) io1_sdio_ctrl;
	    	return qspi.out.io1_sdio_ctrl;
    	endmethod
      interface io_out = interface Get
        method ActionValue#(Bit#(2)) get;
            let temp2 <- qspi.out.io_out.get;
            Bit#(2) temp;
            temp[0] = temp2[0];
            temp[1] = temp2[1];
            return temp;
        endmethod
      endinterface;
      interface io_out_en = interface Get
        method ActionValue#(Bit#(2)) get;
            let temp2 <- qspi.out.io_out_en.get;
            Bit#(2) temp;
            temp[0] = temp2[0];
            temp[1] = temp2[1];
            return temp;
        endmethod
      endinterface;
      interface io_in = interface Put
        method Action put(Bit#(2) in);
            Bit#(4) temp;
            temp[3] = 0;
            temp[2] = 0;
            temp[1] = in[1];
            temp[0] = in[0];
            qspi.out.io_in.put(temp);
        endmethod
      endinterface;
      interface clk_o = qspi.out.clk_o;
      interface ncs_o = qspi.out.ncs_o;
    endinterface;

    interface slave = qspi.slave;

  // 0=TOF, 1=SMF, 2=Threshold, 3=TCF, 4=TEF 5=request_ready
	method Bit#(6) interrupts;
		return qspi.interrupts;
	endmethod

  endmodule
endpackage
