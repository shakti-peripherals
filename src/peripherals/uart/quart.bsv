/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

*  Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------
*/

package quart;

    import AXI4_Lite_Types   :: *;
    import AXI4_Lite_Fabric  :: *;
	import GetPut::*;
	import Uart16550::*;
    `include "instance_defines.bsv"

    (*always_ready, always_enabled*)
    interface QUART_out;
        interface Put#(Bit#(1)) srx_in;
        interface Put#(Bit#(1)) cts_in;
        interface Get#(Bit#(1)) stx_out;
        interface Get#(Bit#(1)) rts_out;
    endinterface

    interface QUART_AXI4_Lite_Ifc;
        interface QUART_out out;
		interface AXI4_Lite_Slave_IFC#(`PADDR,`Reg_width,`USERSPACE) slave;
        (* always_ready, always_enabled *) method Bit#(1) irq;
	endinterface

	(*synthesize*)
    module mkQUART#(Clock core_clock, Reset core_reset)
                                        (QUART_AXI4_Lite_Ifc);

    Uart16550_AXI4_Lite_Ifc uart <- mkUart16550(core_clock, core_reset);

    // ok set up CDC and dsr to 1, and Ring to 0. and otherwise ignore them
	rule rl_put;
        uart.coe_rs232.dsr_in.put(1);
        uart.coe_rs232.dcd_in.put(1);
        uart.coe_rs232.ri_in.put(0);
	endrule
    
    // deliberately drop (ignore) this value
	rule rl_get;
        let temp2 <- uart.coe_rs232.dtr_out.get;
	endrule

    interface out = interface QUART_out
        interface srx_in = interface Put
          method Action put(Bit#(1) in);
            uart.coe_rs232.srx_in.put(in); // RX Input
          endmethod
        endinterface;

        interface cts_in = interface Put
          method Action put(Bit#(1) in);
            uart.coe_rs232.cts_in.put(in); // CTS Input
          endmethod
        endinterface;

        interface stx_out = interface Get
          method ActionValue#(Bit#(1)) get;
            let temp2 <- uart.coe_rs232.stx_out.get;
            Bit#(1) temp = temp2;
            return temp;
          endmethod
        endinterface;

        interface rts_out = interface Get
          method ActionValue#(Bit#(1)) get;
            let temp2 <- uart.coe_rs232.rts_out.get;
            Bit#(1) temp = temp2;
            return temp;
          endmethod
        endinterface;

    endinterface;

    interface slave = uart.slave_axi_uart;

	method Bit#(1) irq;
		return uart.irq;
	endmethod

  endmodule
endpackage
